var express = require('express');
var router = express.Router();
const wrap = require("../../middleware/wrap");

/* GET home page. */
router.get('/', function(req, res, next) {

  //let alphaVantageAPI = require("../helpers/alphavantage/alphavantage")
  
  // Resolving origin of request for allowing client to connect
  // to socket.io within the companies.pug file
  let host = req.get('host');
  let protocol = RegExp('localhost*').test(host) ? "http" : "https"
  let url = protocol + '://' + host;
  res.render('companies', { title: 'Companies', socketLink: url});
});

/* GET companies by string
*
*/
router.get('/:code', wrap(async function(req, res, next) {

    let code= req.params.code;
    //let alphaVantageAPI = require("../helpers/alphavantage/alphavantage")
    // Async operations
    setTimeout(function () {
        console.log("code", code)
        res.json({"code": code});
    }, 2000);
    
    })
);

module.exports = router;