/**
 * @param  {String} port Port Number
 */
module.exports = async function(port) {
//TODO: Live update capabilities
//Getting NGROK key using from env
var ngrokApiKey = process.env.NGROK_APIKEY;
const ngrok = require('ngrok');

/**
   * Exposing NGROK.
   */
  if(ngrokApiKey){
    console.log("Trying to connect to Ngrok");
    
    try{
    var connection = await ngrok.connect({authtoken: ngrokApiKey, port: port});
    console.log("Ngrok connection established at:" , connection);
    
    //Potentially useful later on
    process.env.ngroklink= connection;
    }catch(err){
      console.log("Could not connect to Ngrok");
    }
  }else
  {
    console.log(`Running on localhost`);
  }
};