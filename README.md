# Stonk Exchange

Stock market Alpha Vantage integration (didn't get so far)

### Prerequisites

The application uses Ngrok (optional) for tunneling and integrates Alphavantage
API keys go into the .env file
(TODO on build step make copy of .env.example as .env)

```
If you would like to add your ngrok api key, please make a copy manually.
Assign the key to "NGROK_APIKEY="
```

### Installing


Install dependencies

```
npm install
```

### Running

Within the terminal of your choice, simply run the command below.
```
npm start
```
Given that Ngrok is set up, simply follow the link prompted in the terminal

Add additional notes about how to deploy this on a live system

## Features

### Sockets
The app uses socket.io for communicating between the server and client

Limited usage within [/bin/www](https://gitlab.com/razzle-dazzle/stonk-exchange/blob/master/bin/www)/ , [companies.pug](https://gitlab.com/razzle-dazzle/stonk-exchange/blob/master/views/companies.pug) and [companies.js](https://gitlab.com/razzle-dazzle/stonk-exchange/blob/master/routes/companies/companies.js)

To see the interactions with this page, go to https://*.ngrok.io/companies or localhost:3000/companies 

Both the terminal and the web page will present changes.


### Ngrok
Aimed at quick availablity , ngrok allows for secure https tunnels to local machines. See [ngrok.js](https://gitlab.com/razzle-dazzle/stonk-exchange/blob/master/setup/ngrok.js)

### Setup directory
This [dir](setup) marks where code aimed at running at build and deploy time lays (operations on files, configurations)

## Author

* **Razvan Dragoi**
