const alphaEndpoint="https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=MSFT&apikey=demo";
var requester = require("request");
//const AlphaVantageAPI = require('alpha-vantage-cli').AlphaVantageAPI;

//var yourApiKey = process.env.ALPHAVANTAGE_API_KEY || 'demo';
//var alphaVantageAPI = new AlphaVantageAPI(yourApiKey, 'compact', true);

/**
 * Tries to call alpha
 */
async function getTestAlpha(){
    return new Promise(async(resolve,reject)=> {
        requester(alphaEndpoint, function(error,response,body){
            if(error)
                reject("error");
            console.log(body);
            resolve(JSON.parse(body));
        })
    });
}

module.exports = { 
    test: getTestAlpha
};
